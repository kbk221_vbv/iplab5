window.addEventListener("load", function() {
    let text = document.getElementById("text");
    let red = document.querySelector("#colors>div:nth-child(1)");
    let black = document.querySelector("#colors>div:nth-child(2)");
    let purple = document.querySelector("#colors>div:nth-child(3)");
    let green = document.querySelector("#colors>div:nth-child(4)");
    let yellow = document.querySelector("#colors>div:nth-child(5)");
    let bold = document.querySelector("#font>div:nth-child(1)");
    let cursive = document.querySelector("#font>div:nth-child(2)");
    let underline = document.querySelector("#font>div:nth-child(3)");
    let size = document.getElementById("inp");
    let font = document.querySelector("select");

    red.onclick = function(){
        text.style.color = "#FF0000FF";
    }
    black.onclick = function(){
        text.style.color = "#000000FF";
    }
    purple.onclick = function(){
        text.style.color = "#B404AEFF";
    }
    green.onclick = function(){
        text.style.color = "#04B404FF";
    }
    yellow.onclick = function(){
        text.style.color = "#FFFF00FF";
    }

    bold.onclick = function(){
        if (text.style.fontWeight === "bold"){
            text.style.fontWeight = "normal"
        }
        else{
            text.style.fontWeight = "bold";
        }
    }
    cursive.onclick = function(){
        if (text.style.fontStyle === "italic"){
            text.style.fontStyle = "normal"
        }
        else{
            text.style.fontStyle = "italic";
        }
    }
    underline.onclick = function(){
        if (text.style.textDecoration === "underline"){
            text.style.textDecoration = "none"
        }
        else{
            text.style.textDecoration = "underline";
        }
    }

    size.onchange = function(){
        let x = size.value + "px";
        text.style.fontSize = x;
    }

    font.onchange = function(){
        let x = font.options[font.selectedIndex].text;
        text.style.fontFamily = x;
    }
})