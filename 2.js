const container = document.querySelector('.container');
const containerAddButton = document.querySelector('.container-add-button');

function generatecolor(){
    const hex = Math.floor(Math.random() * 0xffffff).toString(16);
    return `#${hex}`;
}

containerAddButton.addEventListener('click', () => {
    if (container.children.length === 9){
        return;
    }

    const block = document.createElement('div');
    block.style.content = 'dd';
    block.style.border = '1px solid black';
    block.style.backgroundColor = generatecolor();
    block.onclick = () => {
        block.parentElement.removeChild(block);
    };
    container.appendChild(block);
})