let currentColor;

window.addEventListener('click', event => {
    let clickedArea= event.target;
    let clickedTable = event.target.parentNode.parentNode.parentNode;
    let colorTable = document.querySelector('.select-color-table');
    let gameTalbe = document.querySelector('.game-area-table');

    if (clickedTable == colorTable) {
        selectColor(clickedArea);
    } else if (clickedTable == gameTalbe) {
        setColorToGameTable(clickedArea);
    }
});

function selectColor(clickedSquare) {
    let clickedSquareColor = getComputedStyle(clickedSquare).backgroundColor;
    let activeSquare = document.querySelector('.acive-square');

    if (activeSquare != null) {
        activeSquare.classList.remove('acive-square');
    }

    currentColor = clickedSquareColor;
    clickedSquare.classList.toggle('acive-square');
}

function setColorToGameTable(clickedSquare) {
    clickedSquare.style.backgroundColor = currentColor;
}

window.addEventListener('contextmenu', event => {
    let clickedArea = event.target;
    let clickedTable = event.target.parentNode.parentNode.parentNode;
    let gameTable = document.querySelector('.game-area-table');
    
    if (clickedTable == gameTable) {
        clearSquare(clickedArea, event);
    }
});

function clearSquare(clickedSquare, event) {
    clickedSquare.style.backgroundColor = 'white';
    event.preventDefault();
}