let acc = document.getElementsByClassName("accordion");
let i;


for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener("click", function() {
        this.classList.toggle("active");

        let panel = this.nextElementSibling;

        if (panel.style.display === "block") {
            panel.style.display = "none";
        } 
        else {
            deact(acc);
            panel.style.display = "block";
            
        }
    });
}

function deact(acc){
    for (let i = 0; i < acc.length; i++){

        acc[i].classList.toggle("active");
        let panel = acc[i].nextElementSibling;
        panel.style.display = "none";
    }
}